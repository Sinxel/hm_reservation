import 'package:flutter/material.dart';

class ErrorBox extends StatelessWidget{
  String message;
  ErrorBox(this.message);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xFFFFE4E4),
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: Color(0xFFEBC9C9))
        ),
        padding: EdgeInsets.all(20),
        child: Row(
          children: [
            Text(message, style: TextStyle(color: Color(0xFF95514F), fontSize: 16, ),),
            Spacer(),
            Icon(Icons.cancel_outlined, color: Color(0xFF95514F),)
          ],
        ),
      ),
    );
  }

}