import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/screens/hall.dart';

import '../application.dart';

class DiscountView extends StatelessWidget{
  final Hall hall;
  DiscountView(this.hall);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: InkWell(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 120,
          child: Stack(
            children: [
              CachedNetworkImage(
                imageUrl: hall.images.length>0?Application.STORAGE_URL + hall.images[1].path:Application.STORAGE_URL+"no_image.png",
                imageBuilder: (context, imageProvider) => Container(
                  height: 120.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                placeholder: (context, url) => Container(
                  width: 64,
                  height: 64,
                  child: Center(
                    child: SizedBox(
                      width: 64,
                      height: 64,
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.black.withOpacity(1),
                      Colors.black.withOpacity(0)
                    ], begin: Alignment.centerRight, end: Alignment.centerLeft),
                    borderRadius: BorderRadius.circular(8)),
                padding: EdgeInsets.all(20),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        hall.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Reem",
                            fontSize: 18),
                      ),
                      SizedBox(height: 10,),
                      Text("خصم " + hall.discount.toString()+"%", style: TextStyle(color: Colors.white, fontSize: 20, ), textAlign: TextAlign.end,)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        onTap: (){
          Navigator.of(context).push(new MaterialPageRoute(builder: (context)=>HallScreen(hall.id)));
        },
      ),
    );
  }

}