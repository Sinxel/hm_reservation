import 'package:flutter/material.dart';
import 'package:reservation/models/calendar_day.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/screens/day_screen.dart';
import 'package:reservation/values/app_colors.dart';

class EventCalendar extends StatefulWidget{

  final int year;
  final int month;
  final int day;
  final Hall hall;
  final List<dynamic> events;
  EventCalendar(this.year, this.month, this.day, this.hall, this.events);

  @override
  _EventCalendarState createState() => _EventCalendarState();
}

class _EventCalendarState extends State<EventCalendar> {
  DateTime current;
  List<CalendarDay> _days = [];
  bool _isReady  = false;
  var _names = [
    "السبت",
    "الأحد",
    "الإثنين",
    "الثلاثاء",
    "الأربعاء",
    "الخميس",
    "الجمعة"
  ];
  var _months = [
    "كانون ثاني",
    "شباط",
    "آذار",
    "نيسان",
    "أيار",
    "حزيران",
    "تموز",
    "آب",
    "أيلول",
    "تشرين أول",
    "تشرين ثاني",
    "كانون أول"
  ];
  @override
  void initState() {


    current = DateTime(widget.year, widget.month, widget.day);
    DateTime firstDayOfMonthDateTime = DateTime(current.year, current.month, 1);
    DateTime previousMonth = DateTime(current.year, current.month-1, 1);
    DateTime nextMonth = DateTime(current.year, current.month+1);
    int dim = DateUtils.getDaysInMonth(current.year, current.month);
    int pmd = DateUtils.getDaysInMonth(previousMonth.year, previousMonth.month);
    int dayValue = firstDayOfMonthDateTime.weekday+1;
    if(dayValue==7) dayValue = 0;
    for(int i=0; i<42; i++){
      int d = 0;
      if(i<dayValue){
        d = pmd+i-dayValue+1;
        _days.add(CalendarDay(day: d, month: previousMonth.month, year: previousMonth.year, events: 0));
      }else if(i>dim+dayValue-1){
        d = i-dim-dayValue+1;
        _days.add(CalendarDay(day: d, month: nextMonth.month, year: nextMonth.year, events: 0));
      }else{
        d = i-dayValue+1;
        _days.add(CalendarDay(day: d, month: current.month, year: current.year, events: widget.events[d-1]));
      }


    }
    setState(() {
      _isReady = true;
    });
    super.initState();
  }

  _captionView(String label){
    Widget widget =  Text(
      label,
      style: TextStyle(fontSize: 10, color: AppColors.PRIMARY, fontWeight: FontWeight.bold),
    );
    return Padding(padding: EdgeInsets.all(2), child: Container(
      height: 36,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(color: Colors.black.withOpacity(0.1)),
      ),
      child: Center(
        child: widget,
      ),
    ),);
  }
  _dayView(CalendarDay day){

    Widget tw = Text(day.day.toString(), style: TextStyle(color: day.month==current.month&&day.year==current.year?Colors.black:Colors.black.withOpacity(0.5)),);


    return Padding(
        padding: EdgeInsets.all(2),
      child: InkWell(
        child: Container(
          height: 36,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Colors.black.withOpacity(0.1)),
          ),
          child: Stack(
            children: [
              Center(
                child: tw,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: _buildEvents(day.events),
              )
            ],
          ),
        ),
        onTap: (){

          Navigator.of(context).push(new MaterialPageRoute(builder: (context)=>DayScreen(widget.hall, DateTime(day.year, day.month, day.day))));
        },
      ));
  }

  _buildEvents(int noe){
    bool hasMore = false;
    if(noe>3){
      hasMore = true;
    }
    if(noe>2){
      noe = 3;
    }
    List<Widget> _circles = [];
    for(int i=0; i<noe; i++){
      _circles.add(
        Container(
          width: 6,
          height: 6,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: i>1&&hasMore?AppColors.GREEN.withOpacity(0.6):AppColors.PRIMARY.withOpacity(0.6),
          ),
        )
      );
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: _circles,
    );
  }

  _buildWeek(int weekIndex){
    List<Widget> _week = [];
    if(weekIndex==0){
      for(int i=0; i<7; i++){
        _week.add(_captionView(_names[i]));
      }
      return _week;
    }
    for(int i=0; i<7; i++){

      _week.add(_dayView(_selectDay(weekIndex, i)));
    }
    return _week;
  }
  _selectDay(wi, i){
    int index = ((wi-1)*7) + i;
    return _days[index];
  }
  _buildRows(){
    List<TableRow> _rows = [];

    for(int i=0; i<7; i++){
      _rows.add(TableRow(
        children: _buildWeek(i)
      ));
    }
    return _rows;
  }
  @override
  Widget build(BuildContext context) {

    return _isReady?Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Text(_months[widget.month-1]+" " + widget.year.toString(), style: TextStyle(color: AppColors.PRIMARY),),
            Table(
              children: _buildRows(),
            )
          ],
        ),
      ),
    ):SizedBox();
  }


}