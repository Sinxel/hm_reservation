import 'package:flutter/material.dart';

class HallSlide extends StatelessWidget{
  final String image;
  final String hallName;

  HallSlide(this.image, this.hallName);

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        image: DecorationImage(
          image: AssetImage(image),
          fit: BoxFit.cover
        )
      ),
      child: Align(
        alignment: Alignment.topLeft,
        child: Padding(
          padding: EdgeInsets.only(top: 20),
          child: Container(
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width,
            height: 50,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.black,
                    Colors.black.withOpacity(0)]
              ),

            ),
            child: Text(hallName, style: TextStyle(fontSize: 18, fontFamily: 'Reem', color: Colors.red),),
          ),
        ),
      ),
    );
  }

}