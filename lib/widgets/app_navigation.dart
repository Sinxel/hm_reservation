import 'package:flutter/material.dart';
import 'package:reservation/values/route_names.dart';

class AppNavigation extends StatelessWidget{
  final int selectedIndex;
  AppNavigation(this.selectedIndex);
  @override
  Widget build(BuildContext context) {

    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: "الرئيسية",

        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.food_bank),
          label: "القاعات"
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          label: "الإعدادات"
        )
      ],
      onTap: (index){
        if(index!=selectedIndex){
          _openScreen(context, index);
        }
      },
      currentIndex: selectedIndex,
    );
  }

  _openScreen(context, index){
    String routeName = '';
    switch(index){
      case 0:
        routeName = RouteNames.HOME;
        break;
      case 1:
        routeName = RouteNames.HALLS;
        break;
    }
    Navigator.of(context).pushNamed(routeName);
  }

}