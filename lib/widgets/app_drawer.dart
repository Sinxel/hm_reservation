import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/screens/bookings.dart';
import 'package:reservation/screens/hall_form.dart';
import 'package:reservation/screens/home.dart';
import 'package:reservation/screens/splash.dart';
import 'package:reservation/values/app_colors.dart';

class AppDrawer extends StatefulWidget {
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  int _nor;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadReservations();
  }

  _loadReservations() async {
    Dio dio = new Dio(BaseOptions(headers: {
      'accept': 'application/json',
      'authorization': 'Bearer ' + Application().getUser().token
    }));
    dio.get(Application.API_URL + "user/reservations").then((value) {
      setState(() {
        _nor = value.data['total'];
      });
    }).onError((error, stackTrace) {
      print(error.response);
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];
    children.add(DrawerHeader(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "قاعات فلسطين",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30),
            ),
            Text(
              Application().getUser() == null
                  ? ''
                  : Application().getUser().name,
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_DARK, AppColors.PRIMARY_LIGHT],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight),
      ),
    ));

    children.add(ListTile(
      title: Text("الرئيسية"),
      leading: Icon(Icons.home),
      onTap: () {
        Navigator.of(context).pushReplacement(
            new MaterialPageRoute(builder: (context) => HomeScreen()));
      },
    ));
    if (Application().getUser().isHallOwner) {
      children.add(ListTile(
        title: Text("إضافة قاعة"),
        leading: Icon(Icons.add_box_outlined),
        onTap: () {
          _addHall(context);
        },
      ));
      children.add(ListTile(
        title: Text("الحجوزات"),
        leading: Icon(Icons.book),
        trailing: Container(
          decoration: BoxDecoration(
            color: _nor == null || _nor < 1
                ? Colors.transparent
                : AppColors.GREEN.withOpacity(0.6),
            borderRadius: BorderRadius.circular(8),
          ),
          width: 24,
          height: 24,
          child: Center(
            child: _nor == null
                ? CircularProgressIndicator()
                : _nor == 0
                    ? SizedBox()
                    : Text(
                        _nor.toString(),
                        style: TextStyle(color: Colors.white),
                      ),
          ),
        ),
        onTap: () {
          Navigator.of(context)
              .push(new MaterialPageRoute(builder: (context) => Bookings()));
        },
      ));
    }
    children.add(ListTile(
      title: Text("تسجيل الخروج"),
      leading: Icon(Icons.exit_to_app_sharp),
      onTap: () {
        _logout(context);
      },
    ));
    return Drawer(
      child: ListView(
        children: children,
      ),
    );
  }

  _addHall(BuildContext context) {
    Navigator.of(context)
        .push(new MaterialPageRoute(builder: (context) => HallForm()));
  }

  _logout(BuildContext context) {
    Application().setUser(null);
    Navigator.of(context).pushReplacement(
        new MaterialPageRoute(builder: (context) => SplashScreen()));
  }
}
