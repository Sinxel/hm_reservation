class CalendarDay{
  int day;
  int month;
  int year;
  int events;
  CalendarDay({this.day, this.year, this.month, this.events});
}