import 'package:reservation/helper.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/models/user.dart';

class Reservation{
  int id;
  Hall hall;
  User user;
  DateTime startTime;
  DateTime endTime;
  Reservation();

  factory Reservation.fromMap(Map<String, dynamic> map){
    Reservation reservation = new Reservation();
    reservation.id = map['id'];

    reservation.user = User.fromMap(map['user']);
    reservation.hall = Hall.fromMap(map['hall']);
    reservation.startTime = Helper.parseIsoDate(map['startDate']);

    reservation.endTime = Helper.parseIsoDate(map['endDate']);
    return reservation;
  }
}