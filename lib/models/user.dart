class User {
  int id;
  String name;
  String mobile;
  String email;
  String token;
  bool isHallOwner;

  User();

  factory User.fromMap(Map<String, dynamic> map){
    print(map);
    User user = new User();
    user.id = map['id'];
    user.name = map['name'];
    user.mobile = map['mobile'];
    user.email = map['email'];
    user.token = map['api_token'];
    user.isHallOwner = map['hallOwner'];

    return user;
  }
}