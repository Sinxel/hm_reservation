import 'package:reservation/models/gallery_item.dart';
import 'package:reservation/models/user.dart';

class Hall{
  int id;
  String name;
  double latitude;
  double longitude;
  User user;
  String address;
  String phone;
  int priceRange;
  int capacity;
  int discount;
  String comments;
  String email;
  List<GalleryItem> images;

  Hall();

  factory Hall.fromMap(Map<String, dynamic> map){


    Hall hall = new Hall();

    hall.id = map['id'];
    hall.name = map['name'];
    hall.latitude = double.parse(map['latitude']);
    hall.longitude = double.parse(map['longitude']);
    hall.address = map['address'];
    hall.phone = map['phone'];
    hall.priceRange = int.parse(map['priceRange'].toString());
    hall.capacity = int.parse(map['capacity'].toString());
    hall.comments = map['comments'];
    hall.email = map['email'];
    hall.discount = int.parse(map['discount'].toString());

    hall.user = User.fromMap(map['user']);

    List<dynamic> images = map['images'];
    hall.images = [];
    images.forEach((element) {
      hall.images.add(GalleryItem.fromMap(element));
    });


    return hall;
  }

}