class GalleryItem{
  int id;
  String path;
  GalleryItem();
  factory GalleryItem.fromMap(Map<String, dynamic> map){
    GalleryItem item = GalleryItem();
    item.id  = map['id'];
    item.path = map['imagePath'];
    item.path = item.path.substring("public/".length);
    return item;
  }

}