import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/screens/about.dart';
import 'package:reservation/screens/auth_screen.dart';
import 'package:reservation/screens/halls.dart';
import 'package:reservation/screens/home.dart';
import 'package:reservation/screens/splash.dart';
import 'package:reservation/values/route_names.dart';


void main() {

  WidgetsFlutterBinding.ensureInitialized();
  _loadCameras();
  Map<String, WidgetBuilder> routes = new Map();
  routes["about"]  = (BuildContext context){

    return AboutScreen();
  };

  runApp(ResApp());
}
_loadCameras() async{
  Application().cameras = await availableCameras();
}
class ResApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Reservation Application",
      debugShowCheckedModeBanner: false,
      supportedLocales: [Locale('ar', ''), Locale('en', 'US')],
      initialRoute: RouteNames.SPLASH,

      routes: {
        RouteNames.AUTH: (context) => AuthScreen(),
        RouteNames.HOME :    (context) => HomeScreen(),
        RouteNames.ABOUT :   (context) => AboutScreen(),
        RouteNames.HALLS: (context) =>Halls(),
        RouteNames.SPLASH: (context)=>SplashScreen()
      },
    );
  }


}





