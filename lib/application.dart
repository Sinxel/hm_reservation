import 'package:camera/camera.dart';
import 'package:reservation/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Application{
    static final Application _app = Application._internal();
    static const  String API_URL = "http://66.29.143.107/res/public/api/";
    static const  String STORAGE_URL = "http://66.29.143.107/res/public/storage/";
    static const String PREF_TOKEN_KEY = 'token';
    SharedPreferences pref;
    User _user;
    List<CameraDescription> cameras;
    Application._internal();

    initPreferences() async{
        pref = await SharedPreferences.getInstance();
    }

    factory Application(){
        return _app;
    }

    User getUser(){
        return _user;
    }

    loadToken(){
        String token = pref.get(PREF_TOKEN_KEY);
        return token;

    }

    void setUser(User user) async{
        await initPreferences();
        _user = user;
        if(user==null){
            await pref.remove(PREF_TOKEN_KEY);
            return;
        }
        await pref.setString( PREF_TOKEN_KEY, user?.token);
    }
}