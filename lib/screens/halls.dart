import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/widgets/HallView.dart';
import 'package:reservation/widgets/app_drawer.dart';
import 'package:reservation/widgets/app_navigation.dart';

class Halls extends StatefulWidget{
  final String query;
  Halls({this.query=""});
  _HallsState createState() => _HallsState();
}

class _HallsState extends State<Halls>{

  bool _isLoading = true;
  List<Hall> _halls;
  @override
  void initState() {

    super.initState();
    _loadHalls();
  }

  Future<void> _loadHalls() async{
    setState(() {
      _isLoading = true;
      _halls = [];
    });
    Map<String, dynamic> headers = {
      'accept' : 'application/json',
    };
    if(Application().getUser()!=null){
      headers['authorization'] = 'Bearer '+Application().getUser().token;
    }
    BaseOptions options = new BaseOptions(
      headers: headers
    );
    Dio dio = new Dio(options);
    dio.get(Application.API_URL+"halls"+(widget.query!=""?"/"+widget.query:""))
      .then((value){

        List<dynamic> hallsList = value.data['halls'];

        hallsList.forEach((element) {
          _halls.add(Hall.fromMap(element));
        });
        setState(() {
          _isLoading = false;
        });
    }).onError((error, stackTrace){
      print(error.response);
      print(stackTrace);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: AppDrawer(),
        bottomNavigationBar: AppNavigation(1),
        appBar: AppBar(
          title: Text("القاعات"),
        ),
        body: _buildView(),
      ),
    );
  }

  _buildView(){
    if(_isLoading){
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return SafeArea(
      child: RefreshIndicator(
        onRefresh: _loadHalls,
        child: _halls.length>0?ListView.builder(
          padding: EdgeInsets.all(20),
          itemBuilder: (context, index){
          return _buildHallView(index);
        },
          itemCount: _halls.length,
        ):Center(child: Text("لم يتم إيجاد أي قاعة"),),
      ),

    );
  }

  _buildHallView(int index){
    print(index);
    Hall hall = _halls[index];
    return HallView(hall);
  }

}