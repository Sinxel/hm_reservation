import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:reservation/application.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:reservation/widgets/app_drawer.dart';

class HallForm extends StatefulWidget {
  _HallFormState createState() => _HallFormState();
}

class _HallFormState extends State<HallForm> {
  List<String> _images = [];
  ImagePicker _picker;
  double _thumbnailWidth = 213;
  double _thumbnailHeight = 120;
  TextEditingController _txtName;
  TextEditingController _txtAddress;
  TextEditingController _txtPhone;
  TextEditingController _txtCapacity;
  TextEditingController _txtPriceRange;
  TextEditingController _txtEmail;
  TextEditingController _txtComments;
  TextEditingController _discountController;
  Location _location = new Location();
  bool _locationEnabled = false;
  bool _isUploading = false;
  LocationData _locationData;
  double _uploadPercentage = 0;
  @override
  void initState() {
    _picker = ImagePicker();
    _txtName = new TextEditingController();
    _txtAddress = new TextEditingController();
    _txtPhone = new TextEditingController(text: Application().getUser().mobile);
    _txtCapacity = new TextEditingController(text: '0');
    _txtPriceRange = new TextEditingController(text: '0');
    _txtEmail  = new TextEditingController();
    _txtComments = new TextEditingController();
    _discountController  = new TextEditingController(text: "0");
    _checkLocation();
    super.initState();
  }


  _checkLocation() async{
    bool serviceEnabled = await  _location.serviceEnabled();
    if(!serviceEnabled){
      serviceEnabled = await _location.requestService();
      if(!serviceEnabled) Navigator.of(context).pop();
    }
     PermissionStatus status = await _location.hasPermission();
    if(status==PermissionStatus.denied){
      status = await _location.requestPermission();
      if(status!=PermissionStatus.granted){
        Navigator.of(context).pop();
      }
    }
    _locationData = await _location.getLocation();
    setState(() {
      _locationEnabled = true;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text('إضافة قاعة'),
        ),
        drawer: AppDrawer(),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: _buildChild(),
          ),
        ),
      ),
    );
  }

  _buildChild(){
    if(_locationEnabled){
      return SingleChildScrollView(
        child: Column(
          children: _form(),
        ),
      );
    }
    return Center(
      child: CircularProgressIndicator(),
    );
  }
  _form(){
    return [
      Container(
        height: _thumbnailHeight+10,
        padding: EdgeInsets.all(5),
        child: ListView.builder(
          itemBuilder: (context, index) {
            return _imageWidget(index);
          },
          itemCount: _images.length + 1,
          scrollDirection: Axis.horizontal,
        ),
      ),
      _progressBar(),
      SizedBox(height: 10,),
      TextField(
        controller: _txtName,
        decoration: InputDecoration(
          labelText: "إسم القاعة"
        ),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
        controller: _txtPhone,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          labelText: "رقم الهاتف"
        ),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
        controller: _txtEmail,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            labelText: "البريد الالكتروني"
        ),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
        controller: _txtCapacity,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: "سعة القاعة"
        ),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
        controller: _txtPriceRange,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: "السعر يبدأ من"
        ),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
        controller: _txtAddress,
        minLines: 3,
        maxLines: 3,
        decoration: InputDecoration(
          labelText: "عنوان القاعة"
        ),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
        controller: _discountController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: "الخصم",
          helperText: "بين 0 و 100"
        ),
      ),
      TextField(
        controller: _txtComments,
        minLines: 3,
        maxLines: 3,
        decoration: InputDecoration(
            labelText: "ملاحظات"
        ),
      ),
      SizedBox(height: 20,),
      Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(onPressed: (){
            _save();
          }, child: Text("حفظ"), style: ElevatedButton.styleFrom(primary: AppColors.GREEN),),
          ElevatedButton(onPressed: (){
            _cancel();
          }, child: Text("إلغاء"), style: ElevatedButton.styleFrom(primary: AppColors.DANGER),),
        ],
      )
    ];
  }

  _save() async{
    Dio dio = new Dio(
      BaseOptions(
        headers: {
          'accept' : 'application/json',
          'authorization' : 'Bearer '+Application().getUser().token
        }
      )
    );
    FormData data = new FormData();
    data.fields.add(MapEntry('name', _txtName.text));
    data.fields.add(MapEntry('phone', _txtPhone.text));
    data.fields.add(MapEntry('email', _txtEmail.text));
    data.fields.add(MapEntry('capacity', _txtCapacity.text));
    data.fields.add(MapEntry('priceRange', _txtPriceRange.text));
    data.fields.add(MapEntry('address', _txtAddress.text));
    data.fields.add(MapEntry('comments', _txtComments.text));
    data.fields.add(MapEntry('discount', _discountController.text));
    data.fields.add(MapEntry('latitude', _locationData.latitude.toString()));
    data.fields.add(MapEntry('longitude', _locationData.longitude.toString()));

    for(int i=0; i<_images.length; i++){
      data.files.add(MapEntry('image_'+i.toString(), await MultipartFile.fromFile(_images[i])));
    }
    dio.post(Application.API_URL + 'halls/add', data: data).then((value){

      Hall hall = Hall.fromMap(value.data['hall']);

      if(hall.id>0){
        Navigator.of(context).pop();
      }else{
        print("Error");
      }



    }).onError((error, stackTrace){
      print(error.response);
      print(stackTrace);
    });

    
  }

  _cancel(){
    Navigator.of(context).pop();
  }
  Widget _progressBar(){
      return Padding(
        padding: EdgeInsets.only(top: 5),
        child: LinearProgressIndicator(
          value: _uploadPercentage,
        ),
      );
  }
  _imageWidget(int index) {
    if (index == _images.length) {
      return _addImage();
    }
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
                image: FileImage(File(_images[index])), fit: BoxFit.cover)),
        width: _thumbnailWidth,
        height: _thumbnailHeight,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: IconButton(
                icon: Icon(Icons.remove_circle, color: AppColors.DANGER, size: 36,),
                onPressed: (){
                  _images.removeAt(index);
                  setState(() {

                  });
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  _addImage() {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.SUCCESS,
        borderRadius: BorderRadius.circular(8),
      ),
      width: _thumbnailWidth,
      height: _thumbnailHeight,
      child: Center(
        child: PopupMenuButton(
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                child: Text("من معرض الصور"),
                value: 0,
              ),
              PopupMenuItem(
                child: Text("من الكاميرا"),
                value: 1,
              )
            ];
          },
          child: Icon(
            Icons.add_circle,
            size: 36,
            color: AppColors.SUCCESS_DARK,
          ),
          onSelected: (value) {
            if (value == 0) {
              _pickFromGallery();
            } else {
              _pickFromCamera();
            }
          },
        ),
      ),
    );
  }

  _pickFromCamera() async{
    PickedFile _image = await _picker.getImage(source: ImageSource.camera);
    if (_image != null) {
      _images.add(_image.path);
      setState(() {});
    }
  }

  _pickFromGallery() async {
    PickedFile _image = await _picker.getImage(source: ImageSource.gallery);
    if (_image != null) {
      _images.add(_image.path);
      setState(() {});
    }
  }
}
