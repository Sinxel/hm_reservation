import 'dart:ui';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/models/user.dart';
import 'package:reservation/screens/home.dart';
import 'package:reservation/values/app_colors.dart';

class AuthScreen extends StatefulWidget {
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  var txtMobile = TextEditingController();
  var txtPassword = TextEditingController();
  var txtName = TextEditingController();
  String _caption = "دخول";
  bool _isLogin = true;
  String _error;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        _buildBackground(),
        _buildForm(),
      ],
    ));
  }

  _buildBackground() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [AppColors.PRIMARY_DARK, AppColors.PRIMARY_LIGHT],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight)),
    );
  }

  _buildForm() {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 40),
              height: MediaQuery.of(context).size.height - 60,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      "assets/images/logo.png",
                      width: 156,
                    ),
                    Text(
                      _caption,
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    TextField(
                      controller: txtMobile,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white.withOpacity(0.1),
                        labelText: 'الجوال',
                        border: OutlineInputBorder(),
                        prefixIcon: Icon(Icons.phone),
                      ),
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    _isLogin
                        ? SizedBox()
                        : Padding(
                            padding: EdgeInsets.only(bottom: 15),
                            child: TextField(
                              controller: txtName,

                              decoration: InputDecoration(
                                labelText: 'الإسم',
                                border: OutlineInputBorder(),
                                fillColor: Colors.white.withOpacity(0.1),
                                filled: true,
                                prefixIcon: Icon(Icons.account_box_rounded),

                              ),
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                    TextField(
                      controller: txtPassword,
                      obscureText: _isLogin,
                      decoration: InputDecoration(
                        labelText: 'كلمة المرور',
                        border: OutlineInputBorder(),
                        fillColor: Colors.white.withOpacity(0.1),
                        filled: true,
                        prefixIcon: Icon(Icons.lock),
                        suffixIcon: Icon(Icons.remove_red_eye_sharp),
                      ),
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    _error == null
                        ? SizedBox(
                            height: 8,
                          )
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                padding: EdgeInsets.all(4),
                                color: AppColors.DANGER,
                                child: Text(
                                  _error,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                            ],
                          ),
                    Container(
                        width: double.infinity,
                        height: 35,
                        color: Colors.blue,
                        child: MaterialButton(
                          onPressed: () {
                            setState(() {
                              _error = null;
                            });
                            if (_isLogin) {
                              _attemptLogin();
                            } else {
                              _attemptRegister();
                            }
                          },
                          child: Text(
                            _caption,
                            style: TextStyle(color: Colors.white),
                          ),
                        )),
                    TextButton(
                        onPressed: () {
                          _changeForm();
                        },
                        child: Text(_isLogin ? 'مستخدم جديد' : "لدي حساب")),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  _changeForm() {
    _isLogin = !_isLogin;
    if (_isLogin) {
      _caption = "دخول";
    } else {
      _caption = "تسجيل";
    }
    setState(() {});
  }

  _attemptLogin() async {

    Dio dio = new Dio(BaseOptions(headers: {'accept': 'application/json'}));
    dio.post(Application.API_URL + "login", data: {
      "mobile": txtMobile.text,
      "password": txtPassword.text
    }).then((value) {
      var user = User.fromMap(value.data['user']);
      Application().setUser(user);
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
    }).timeout(Duration(seconds: 5), onTimeout: () {
      setState(() {
        _error = "لم نتمكن من الإتصال بالخادم";
      });
    }).onError((DioError error, stackTrace) {
      setState(() {
        _error = error.response.data['message'];
      });
    });
  }

  _attemptRegister() async {
    Dio dio = new Dio(BaseOptions(headers: {'accept': 'application/json'}));
    dio.post(Application.API_URL + "register", data: {
      "mobile": txtMobile.text,
      "password": txtPassword.text,
      "name": txtName.text
    }).then((value) {
      var user = User.fromMap(value.data['user']);
      Application().setUser(user);
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
    }).onError((DioError error, stackTrace) {
      print(error.response);
      setState(() {
        _error = error.response.data['message'];
      });
    });
  }
}
