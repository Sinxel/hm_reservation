import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      appBar: AppBar(
        title: Text("About"),
      ),
      body: SafeArea(

        child: Container(

          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: Column(
            children: [
              Text("I am Ra'ed"),
              TextButton(child: Text("Back"), onPressed: (){
                Navigator.of(context).pop();
              },)
            ],
          ),
        ),
      ),
    );
  }

}