import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/helper.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/screens/reserve.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:reservation/widgets/app_drawer.dart';
import 'package:reservation/widgets/app_navigation.dart';

import '../application.dart';

class DayScreen extends StatefulWidget{
  final DateTime day;
  final Hall hall;
  DayScreen(this.hall, this.day);
  _DayScreenState createState() => _DayScreenState();
}

class _DayScreenState extends State<DayScreen>{

  List<dynamic> _events;
  @override
  void initState() {
    // TODO: implement initState
    _loadEvents();
  }

  _loadEvents() {
    Dio dio = new Dio(
        BaseOptions(
            headers: {
              'accept' : 'application/json',
              'authorization' : 'Bearer '+Application().getUser().token
            }
        )
    );
    dio.get(Application.API_URL+"dayEvents/"+widget.hall.id.toString()+"/"+Helper.formatDay(widget.day)).then((value){
      setState(() {

        _events = value.data['events'];
        print(_events);
      });

    }).onError((error, stackTrace){
      print(error.response);
      print(error);
    });
  }
  @override
  Widget build(BuildContext context) {

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: AppDrawer(),
        bottomNavigationBar: AppNavigation(0),
        appBar: AppBar(
          title: Text("حجوزات"),
        ),
        body: _buildView(),
      ),
    );
  }

  _buildView(){
    if(_events==null){
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: AppColors.PRIMARY,
              borderRadius: BorderRadius.circular(8),

            ),
            padding: EdgeInsets.all(10),
            height: 80,
            child: Stack(
              children: [
                Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(widget.hall.name, style: TextStyle(color: Colors.white),),
                      SizedBox(height: 4,),
                      Text(_dayName(widget.day.weekday)+"، " + widget.day.day.toString() + "/" + widget.day.month.toString() + "/" + widget.day.year.toString(), style: TextStyle(color: Colors.white), )
                    ],
                  ),
                ),
                /*
                Align(
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white.withOpacity(0.6)
                          ),
                          width: 24,
                          height: 24,
                          child: Center(
                            child: Icon(Icons.arrow_left, color: AppColors.PRIMARY,),
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white.withOpacity(0.6)
                          ),
                          width: 24,
                          height: 24,
                          child: Center(
                            child: Icon(Icons.arrow_right, color: AppColors.PRIMARY,),
                          ),
                        ),
                      ),

                    ],
                  ),
                )*/
              ],
            ),
          ),
          Expanded(
            child: _buildList(),
          )
        ],
      ),
    );
  }

  bool _getReserved(int h){
    for(int i=0; i<_events.length; i++){
      Map<String, dynamic> event = _events[i];
      DateTime start = Helper.parseIsoDate(event['start']);
      DateTime end = Helper.parseIsoDate(event['end']);
      if(h>=start.hour&&h<=end.hour) return true;
    }
    return false;
  }
  _buildList(){
    List<Widget> children = [];

    children.add(SizedBox(height: 10,));
    for(int i=0; i<24; i++) {
      String time = "12:00 ص";
    if(i==0){
      time = "12:00 ص";
    }else if(i<10&&i>0){
        time = "0" + i.toString() + ":00 ص";
      }else if(i<13&&i>9){
        time = i.toString() + ":00 ص";
      }else if(i<22){
        time = "0" + (i-12).toString() + ":00 م";
      }else{
        time = (i-12).toString() + ":00 م";
      }
      bool isReserved = _getReserved(i);
      children.add(
        ListTile(
          tileColor: isReserved?AppColors.GREEN.withOpacity(0.4):Colors.white,
          leading: Text(time, style: TextStyle(color: AppColors.PRIMARY, fontSize: 12),),
          title: Container(

            child: Text(isReserved?"محجوز":"اضغط لإضافة حجز في هذه الساعة", style: TextStyle(fontSize: 12),),
            
          ),
          onTap: (){
            if(isReserved) return;
            Navigator.of(context).push(new MaterialPageRoute(builder: (context)=>Reserve(widget.hall, widget.day, i)));
          },

        )
      );
      children.add(Container(height: 1, color: Colors.black.withOpacity(0.2),));
    }
    return ListView(
      children: children,
    );
  }

  _dayName(int index){
    var names = [
      "السبت",
      "الأحد",
      "الإثنين",
      "الثلاثاء",
      "الأربعاء",
      "الخميس",
      "الجمعة"
    ];
    if(index==6) return names[0];
    return names[index+1];
  }

}
