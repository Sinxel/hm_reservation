import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/screens/hall.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:reservation/widgets/app_drawer.dart';
import 'package:reservation/widgets/app_navigation.dart';

class Reserve extends StatefulWidget{
  final Hall hall;
  final DateTime day;
  final int hour;
  Reserve(this.hall,this.day,this.hour);
  _ReserveState createState() => _ReserveState();
}

class _ReserveState extends State<Reserve>{
  int _selectedPeriod = 2;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: AppDrawer(),
        bottomNavigationBar: AppNavigation(0),
        appBar: AppBar(
          title: Text("إضافة حجز"),
        ),
        body: _buildView(),
      ),
    );
  }

  _buildView(){

    return _form();
  }

  _form(){
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            dataRow("الحجز في قاعة", widget.hall.name),
            dataRow("يوم", widget.day.day.toString() + "/" + widget.day.month.toString() + "/" + widget.day.year.toString()),
            dataRow("الساعة", _parseHour()),
            dataRow("الحجز بإسم", Application().getUser().name),

            SizedBox(height: 10,),
            Text("الحجز لمدة", style: TextStyle(fontWeight: FontWeight.bold),),
            _periods(),
            SizedBox(height: 10,),
            Text("ملاحظات", style: TextStyle(fontWeight: FontWeight.bold),),
            TextField(
              minLines: 3,
              maxLines: 3,
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(onPressed: (){
                  _save();
                }, child: Text("حفظ"), style: ElevatedButton.styleFrom(primary: AppColors.GREEN),),
                ElevatedButton(onPressed: (){
                  Navigator.of(context).pop();
                }, child: Text("إلغاء"), style: ElevatedButton.styleFrom(primary: AppColors.DANGER),),
              ],
            )
          ],
        ),
      ),
    );
  }

  _save(){
    Dio dio = new Dio(
        BaseOptions(
            headers: {
              'accept' : 'application/json',
              'authorization' : 'Bearer '+Application().getUser().token
            }
        )
    );
    dio.post(Application.API_URL+"halls/reserve", data: {
      'id' : widget.hall.id,
      'user': Application().getUser().id,
      'day' : widget.day.toIso8601String(),
      'time' : widget.hour,
      'duration' : _selectedPeriod
    }).then((value){
      if(value.data['id']>0){
        Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (context)=>HallScreen(widget.hall.id)));
      }
    }).onError((error, stackTrace){
      print(error);
    });
  }
  _periods(){
    List<DropdownMenuItem> items = [
      DropdownMenuItem(child: Text("ساعة"), value: 1,),
      DropdownMenuItem(child: Text("ساعتين"), value: 2,),
      DropdownMenuItem(child: Text("3 ساعات"), value: 3,),
      DropdownMenuItem(child: Text("4 ساعات"), value: 4,),
      DropdownMenuItem(child: Text("5 ساعات"), value: 5,),
    ];
    return DropdownButton(
      items: items,
      value: _selectedPeriod,
      isExpanded: true,
      onChanged: (value){
        setState(() {
          _selectedPeriod = value;
        });
      },
    );
  }

  _parseHour(){
    if(widget.hour==0){
      return "12:00 ص";
    }else if(widget.hour<10){
      return "0" + widget.hour.toString() +  ":00 ص";
    }else if(widget.hour<12){
      return widget.hour.toString() + ":00 ص";
    }else if(widget.hour<22){
      return "0" + (widget.hour-12).toString() + ":00 م";
    }else{
      return (widget.hour-12).toString() + ":00 م";
    }
  }

  dataRow(String key, String value){
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(key),

        Text(value, style: TextStyle(fontWeight: FontWeight.bold),)
      ],
    );
  }

}