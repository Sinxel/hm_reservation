import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/models/reservation.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:reservation/widgets/app_drawer.dart';
import 'package:reservation/widgets/app_navigation.dart';

import '../application.dart';
import '../helper.dart';

class Bookings extends StatefulWidget{
  _BookingsState  createState() => _BookingsState();
}

class _BookingsState extends State<Bookings>{
  List<Reservation> _reservations;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadEvents();
  }
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: AppDrawer(),
        bottomNavigationBar: AppNavigation(0),
        appBar: AppBar(
          title: Text("الحجوزات"),
        ),
        body: _buildView(),
      ),
    );
  }

  _loadEvents() async{
    setState(() {
      _reservations = null;
    });
    Dio dio = new Dio(
        BaseOptions(
            headers: {
              'accept' : 'application/json',
              'authorization' : 'Bearer '+Application().getUser().token
            }
        )
    );
    dio.get(Application.API_URL+"user/events").then((value){
      List<dynamic> events = value.data['events'];
      _reservations = [];
      events.forEach((element) {

        _reservations.add(Reservation.fromMap(element));
      });
      setState(() {

      });


    }).onError((error, stackTrace){
      print(error.response);
      print(error);
    });
  }
  _buildView(){
    if(_reservations==null){
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return ListView.builder(
      itemCount: _reservations.length,
      itemBuilder: (context, index){
        return _booking(_reservations[index]);
      },
    );
  }

  _booking(Reservation reservation){
    return ListTile(
      title: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: Colors.black.withOpacity(0.4)),
        ),
        child: Column(
          children: [
            dataRow("القاعة", reservation.hall.name),
            dataRow("إسم الزبون", reservation.user.name),
            dataRow("رقم الهاتف", reservation.user.mobile),
            dataRow("اليوم", Helper.formatDay(reservation.startTime)),
            dataRow("من", Helper.formatHour(reservation.startTime)),
            dataRow("إلى", Helper.formatHour(reservation.endTime)),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(onPressed: (){
                  _accept(reservation.id);
                }, child: Text("قبول"), style: ElevatedButton.styleFrom(primary: AppColors.GREEN),),
                ElevatedButton(onPressed: (){
                  _reject(reservation.id);
                }, child: Text("رفض"), style: ElevatedButton.styleFrom(primary: AppColors.DANGER),),
              ],
            )
          ],
        ),
      ),
    );
  }

  _reject(id){
    Dio dio = new Dio(
        BaseOptions(
            headers: {
              'accept' : 'application/json',
              'authorization' : 'Bearer '+Application().getUser().token
            }
        )
    );
    dio.get(Application.API_URL+"event/reject/"+id.toString()).then((value){
      _loadEvents();

    }).onError((error, stackTrace){
      print(error.response);
      print(error);
    });
  }

  _accept(id){
    Dio dio = new Dio(
        BaseOptions(
            headers: {
              'accept' : 'application/json',
              'authorization' : 'Bearer '+Application().getUser().token
            }
        )
    );
    dio.get(Application.API_URL+"event/accept/"+id.toString()).then((value){
      _loadEvents();

    }).onError((error, stackTrace){
      print(error.response);
      print(error);
    });
  }

  dataRow(String key, String value) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(key),

        Text(value, style: TextStyle(fontWeight: FontWeight.bold),)
      ],
    );
  }

}