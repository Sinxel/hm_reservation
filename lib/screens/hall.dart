import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:reservation/widgets/app_drawer.dart';
import 'package:reservation/widgets/app_navigation.dart';
import 'package:reservation/widgets/event_calendar.dart';


class HallScreen extends StatefulWidget {
  final int id;
  HallScreen(this.id);
  _HallState createState() => _HallState();
}

class _HallState extends State<HallScreen>  with SingleTickerProviderStateMixin{
  bool _isLoading = false;
  Hall _hall;
  String _title = "جاري التحميل";
  TabController _tabController ;
  List<dynamic> _calendarEvents;
  DateTime currentDate;
  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
    currentDate = DateTime.now();
    _loadHall();
  }

  _loadEvents(int year, int month){
    setState(() {
      _calendarEvents = null;
    });

    Map<String, dynamic> headers = {
      'accept': 'application/json',
    };

    if (Application().getUser() != null) {
      headers['authorization'] = 'Bearer ' + Application().getUser().token;
    }
    BaseOptions options = new BaseOptions(headers: headers);

    Dio dio = new Dio(options);
    dio.get(Application.API_URL + "events/" + _hall.id.toString()+"/"+year.toString()+"/"+month.toString()).then((value){
      print(value);
      setState(() {
        _calendarEvents = value.data['events'];
      });
    }).onError((error, stackTrace){
      print(error.response);
      print(stackTrace);
    });
  }
  _loadHall() async {
    setState(() {
      _isLoading = true;
      _hall = null;
    });
    Map<String, dynamic> headers = {
      'accept': 'application/json',
    };

    if (Application().getUser() != null) {
      headers['authorization'] = 'Bearer ' + Application().getUser().token;
    }
    BaseOptions options = new BaseOptions(headers: headers);

    Dio dio = new Dio(options);
    dio.get(Application.API_URL + "hall/" + widget.id.toString()).then((value) {
      _hall = Hall.fromMap(value.data['hall']);
      setState(() {
        _isLoading = false;
        _title = _hall.name;
      });

      _loadEvents(currentDate.year, currentDate.month);
    }).onError((error, stackTrace) {
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: AppDrawer(),
        bottomNavigationBar: AppNavigation(1),
        appBar: AppBar(
          title: Text(_title),
        ),
        body: _buildView(),
      ),
    );
  }

  _buildView() {
    return _isLoading ? _buildLoading() : _buildHall();
  }

  _buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildHall() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            _carousel(),
            _infoTab(),
          ],
        ),
      ),
    );
  }

  _infoTab(){
    return Expanded(
      child: Column(
        children: [
          TabBar(
            controller: _tabController,
            unselectedLabelColor: Colors.black,
            labelColor: AppColors.PRIMARY,
            tabs: [
              Tab(child: Text("المعلومات"),),

              Tab(child:  Text("الحجز"),),

            ],

          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                _buildInfoView(),

                _buildCalendar(),
              ],
            ),
          )
        ],
      ),
    );
  }

  _buildCalendar(){
    

    if(_calendarEvents==null){
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.all(20),
          child: EventCalendar(currentDate.year, currentDate.month, currentDate.day, _hall, _calendarEvents),
        ),
        Align(
          alignment: Alignment.center,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColors.PRIMARY.withOpacity(0.6)
                  ),
                  width: 24,
                  height: 24,
                  child: Center(
                    child: Icon(Icons.arrow_left, color: Colors.white,),
                  ),
                ),
                onTap: _previousMonth,
              ),
              InkWell(
                child: Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.PRIMARY.withOpacity(0.6)
                  ),
                  width: 24,
                  height: 24,
                  child: Center(
                    child: Icon(Icons.arrow_right, color: Colors.white,),
                  ),
                ),
                onTap: _nextMonth,
              ),

            ],
          ),
        )
      ],
    );
  }
  _nextMonth(){
    currentDate = new DateTime(currentDate.year, currentDate.month+1);
    setState(() {
      _calendarEvents = null;

    });
    _loadEvents(currentDate.year, currentDate.month);
    print(currentDate);
  }
  _previousMonth(){
    currentDate = new DateTime(currentDate.year, currentDate.month-1);
    setState(() {
      _calendarEvents = null;

    });
    _loadEvents(currentDate.year, currentDate.month);
    print(currentDate);
  }
  _buildInfoView(){
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _infoRow("إسم المالك", _hall.user.name??_hall.user.mobile),
            _infoRow("رقم الهاتف", _hall.phone),
            _infoRow("السعة", _hall.capacity.toString()),
            _infoRow("السعر يبدأ من", _hall.priceRange.toString()),
            _infoRow("العنوان", _hall.address),
            SizedBox(height: 10,),
            Text("معلومات إضافية"),
            Text(_hall.comments)

          ],
        ),
      ),
    );
  }

  _infoRow(label, info){
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(label),
        Spacer(),
        Text(info)
      ],
    );
  }
  _carousel() {
    return CarouselSlider(
      items: _buildImages(),
      options: CarouselOptions(
        height: 240,
        aspectRatio: 16/9,
        autoPlay: true,
        enlargeCenterPage: true

      ),
    );
  }

  _buildImages() {
    List<Widget> _images = [];
    _hall.images.forEach((element) {
      _images.add(
        Container(
          child: CachedNetworkImage(
            imageUrl: Application.STORAGE_URL + element.path,
            progressIndicatorBuilder: (context, url, downloadProgress) {
              return CircularProgressIndicator(
                value: downloadProgress.progress,
              );
            },
            errorWidget:(context, url, error)=> Icon(Icons.error),
            fit: BoxFit.cover,
          ),
        ),
      );
    });
    return _images;
  }
}
