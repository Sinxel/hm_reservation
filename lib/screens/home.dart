import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/models/hall.dart';
import 'package:reservation/screens/halls.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:reservation/widgets/app_drawer.dart';
import 'package:reservation/widgets/app_navigation.dart';
import 'dart:ui' as ui;

import 'package:reservation/widgets/discount_view.dart';

class HomeScreen extends StatefulWidget{

   _HomeScreenState createState(){
     return _HomeScreenState();
   }
}
class _HomeScreenState extends State<HomeScreen>{

  TextEditingController _txtSearch;
  List<Hall> _halls;
  @override
  void initState() {

    super.initState();
    _txtSearch = new TextEditingController();
    _loadHome();

  }

   _loadHome() async{
    setState(() {
      _halls = null;
    });
    Map<String, dynamic> headers = {
      'accept' : 'application/json',
    };
    BaseOptions options = new BaseOptions(
        headers: headers
    );
    Dio dio = new Dio(options);
    dio.get(Application.API_URL+"home")
        .then((value){
          _halls = [];
      List<dynamic> hallsList = value.data['halls'];
      hallsList.forEach((element) {
        _halls.add(Hall.fromMap(element));
      });

      setState(() {

      });
    }).onError((error, stackTrace){
      print(error);
      print(error.response);
    });
  }

  @override
  build(BuildContext context){

    List<Widget> children = [];
    children.add(_searchBox());
    if(_halls==null){
      children.add(Container(
        height: 100,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ));
    }else{
      children.add(SizedBox(height: 20,))  ;
      _halls.forEach((element) {
        children.add(DiscountView(element));
      });
    }
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text("حجوزات"),
        ),
        body:  Padding(
          padding: EdgeInsets.all(20),
          child: ListView(
            children: children,
          ),
        ),
        drawer: AppDrawer(),
        bottomNavigationBar: AppNavigation(0),
      ),
    );
  }

  _searchBox(){


    return TextField(
      controller: _txtSearch,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white.withOpacity(0.1),
        hintText: "بحث",
        border: OutlineInputBorder(),
        isDense: true,
        suffix: ElevatedButton.icon(
          icon: Icon(Icons.search),
          onPressed: (){
            if(_txtSearch.text.trim()=="") return;
            Navigator.of(context).push(new MaterialPageRoute(builder: (context)=>Halls(query: _txtSearch.text,)));
          }, label: Text("بحث"),
        )
      )
      ,);
  }
  @override
  void dispose(){

    super.dispose();
  }



}
//FF00FF00

