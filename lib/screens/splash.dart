import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:reservation/application.dart';
import 'package:reservation/widgets/error_box.dart';
import 'package:reservation/models/user.dart';
import 'package:reservation/screens/auth_screen.dart';
import 'package:reservation/screens/home.dart';
import 'package:reservation/values/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String _error;
  @override
  void initState() {
    super.initState();
    _checkToken();
  }


  _checkToken() async {
    setState(() {
      _error = null;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String token = prefs.getString(Application.PREF_TOKEN_KEY);

    if (token == null) {
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => AuthScreen()));
      return;
    }
    Dio dio = new Dio(BaseOptions(headers: {
      'accept': 'application/json',
      'authorization': 'Bearer ' + token
    }));
    dio.get(Application.API_URL + 'auth').then((value) {
      var user = User.fromMap(value.data['user']);
      Application().setUser(user);
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => HomeScreen()));
    }).onError((DioError error, stackTrace) {
      _error = "حدث خطأ ما، يرجى إعادة المحاولة";
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => AuthScreen()));
    }).timeout(Duration(seconds: 5), onTimeout: () {
      //إذا لم يتم الرد من الخادم خلال 5 ثواني فيعني أن الخادم لا يعمل
      setState(() {
        _error = "لم نتمكن من الإتصال بالخادم";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [AppColors.PRIMARY_DARK, AppColors.PRIMARY_LIGHT],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight)),
        child: Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              'assets/images/logo.png',
              width: 240,
            ),
            Text(
              "Reservation\nحجوزات",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Reem'),
              textAlign: TextAlign.center,
            ),
            _error == null
                ? Padding(
                    padding: EdgeInsets.all(20),
                    child: SizedBox(
                      width: 36,
                      height: 36,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    ),
                  )
                : InkWell(
              child: ErrorBox(_error),
              onTap: (){
                _checkToken();
              },
            )
          ],
        )),
      ),
    );
  }
}
