class Helper{
  static String formatDay(DateTime time){
    String y = time.year.toString();
    String m = time.month<10?"0"+time.month.toString():time.month.toString();
    String d = time.day<10?"0"+time.day.toString():time.day.toString();
    return y+"-" + m + "-" + d;
  }

  static String formatHour(DateTime time){
    int h = time.hour<13?time.hour:time.hour-12;
    String s = time.hour<12?"ص":"م";
    String m = time.minute<10?"0"+time.minute.toString():time.minute.toString();
    String t = (h<10?"0"+h.toString():h.toString())+":" + m + " " + s;
    return t;
  }

  static DateTime parseIsoDate(String stamp){

    List<String> dateTimeStamp = stamp.split(" ");

    String dateStamp = dateTimeStamp[0];
    String timeStamp = dateTimeStamp[1];
    List<String> dateSegs = dateStamp.split('-');
    List<String> timeSegs = timeStamp.split(':');

    int y = int.parse(dateSegs[0]);
    int m = int.parse(dateSegs[1]);
    int d = int.parse(dateSegs[2]);
    int h = int.parse(timeSegs[0]);
    int mn = int.parse(timeSegs[1]);
    return DateTime(y, m, d, h, mn);
  }

}