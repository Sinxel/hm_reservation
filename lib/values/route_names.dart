class RouteNames{
  static const String ABOUT = "about";
  static const String HOME = "home";
  static const String AUTH = "auth";

  static const String SPLASH = 'splash';
  static const String HALLS = 'halls';
}