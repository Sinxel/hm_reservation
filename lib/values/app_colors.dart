import 'package:flutter/material.dart';

class AppColors{
  static const Color PRIMARY = Color(0xFF195794);
  static const Color PRIMARY_DARK = Color(0xFF23396C);
  static const Color PRIMARY_LIGHT = Color(0xFF25586A);
  static const Color CANVAS = Color(0xFFe2ecf0);
  static const Color  DANGER = Color(0xFFCC0000);
  static const Color SUCCESS = Color(0xFFD5E8D4);
  static const Color SUCCESS_DARK = Color(0xFF5C655C);
  static const Color GREEN = Color(0xFF00CC00);
}